# Robocode

## Pontos fortes
 * O robô fica em movimento para não ser atingido facilmente, já que o scan localiza ele em apenas uma posição.
 * Possui poder de força elevado, mas o suficiente para ganhar **pontos no placar final**  e não perder tanto poder de fogo.
 * Possui recuo ao ver o inimigo.
 
## Potos fracos
 * O tiro com maior potência acaba rapidamente com seu poder de fogo.

## Aprendizado
 * Observar que para o projeto em sí não basta o robô mais forte, nem o que atira mais, muito menos o único sobrevivente, pois deve haver o balanceamento no resultado final. O mais legal foi criar estratégias, para achar uma melhor solução, e com isso a melhor estrategia para um resultado no placar final.