package Solutis;
import robocode.*;
import robocode.HitRobotEvent;
import robocode.Robot;
import robocode.ScannedRobotEvent;
import java.awt.*;
//import java.awt.Color;

// API help : https://robocode.sourceforge.io/docs/robocode/robocode/Robot.html

/**
 * SMLRobot - a robot by (your name here)
 */
public class SMLRobot extends Robot
{
	/**
	 * run: SMLRobot's default behavior
	 */
	boolean peek; // Olhar se há outro robô por perto
	double moveAmount; // Enquanto mover

	/**
	 * run: Move around the walls
	 */
	public void run() {
		// Set colors
		setBodyColor(Color.black);
		setGunColor(Color.black);
		setRadarColor(Color.red);
		setBulletColor(Color.red);

		// Initializa a variável moveAmount com o máximo possível de movimento para o campo de batalha.
		moveAmount = Math.max(getBattleFieldWidth(), getBattleFieldHeight());
		// Initialize peek to false.
		peek = false;

		// Virar à esquerda para uma parede.
		// Girar a arma 90 graus à direita.
		// Olhar se há robô. 
		turnLeft(getHeading() % 90);
		ahead(moveAmount);
		
		peek = true;
		turnGunRight(90);
		turnRight(90);

		while (true) {
			// Olhe antes de virar quando ahead () for concluído.
			peek = true;
			// Andar na parede do campo de batalha.
			ahead(moveAmount);
			peek = false;
			// Virar para a próxima parede.
			turnRight(90);
		}
	}

	/**
	 * onHitRobot:  Afastar um pouco.
	 */
	public void onHitRobot(HitRobotEvent e) {
		// Se ele estiver na nossa frente, recue um pouco.
		if (e.getBearing() > -90 && e.getBearing() < 90) {
			back(100);
		} // senão, então avance um pouco.
		else {
			ahead(100);
		}
	}

	/**
	 * onScannedRobot:  ATIRE!
	 */
	public void onScannedRobot(ScannedRobotEvent e) {
		fire(5);
		// A varredura é chamada automaticamente quando o robô está se movendo, mas garantimos gerar outro evento de varredura se houver um robô próximo.
		if (peek) {
			scan();
		}
	}
}
